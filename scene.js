import * as THREE from 'three';

var geometry = new THREE.BoxGeometry(1, 1, 1);
var material = new THREE.MeshStandardMaterial({ color: 0x00ff00 });
export const cube = new THREE.Mesh(geometry, material);

export const axesHelper = new THREE.AxesHelper(5);

export const spotLight = new THREE.SpotLight(0xffffff);
spotLight.position.set(-4, 6, -10);
spotLight.castShadow = true;

export const plane = new THREE.Mesh(
  new THREE.PlaneGeometry(60, 30),
  new THREE.MeshStandardMaterial({
    color: 0xffffff,
    metalness: 0,
    side: THREE.DoubleSide,
  })
);

plane.rotation.x = Math.PI / 2;
