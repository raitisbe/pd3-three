import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {cube, spotLight, axesHelper, plane} from './scene.js';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

const renderer = new THREE.WebGLRenderer();
const controls = new OrbitControls(camera, renderer.domElement);
renderer.setClearColor(new THREE.Color(0xeeeeee));
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
scene.add(cube, spotLight, axesHelper, plane);
camera.position.set(10, 10, 10);
camera.lookAt(cube.position);

function animate() {
  cube.rotation.x += 0.1;
  cube.rotation.y += 0.1;
  controls.update();
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
animate();
